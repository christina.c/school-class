let student1 = new Student('caroline', 17, 'promo 11');
let student2 = new Student('george', 17, 'promo 11');
let student3 = new Student('jeny', 17, 'promo 11');
let student4 = new Student('melissa', 17, 'promo 11');
let student5 = new Student('alfred', 17, 'promo 11');

let teacher1 = new Teacher('boris', 'math');
let teacher2 = new Teacher('elise', 'français');

let school = new School('simplon')
school.students.push(student1);
school.students.push(student2);
school.teachers.push(teacher1);
school.teachers.push(teacher2);

console.log(school);
school.firedTeacher(teacher1);
school.graduate(student1);

console.log(school);


