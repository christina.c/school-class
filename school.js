class School{
    constructor(value,){
        this.name = value;
        this.teachers = [];
        this.students = [];
    }
    firedTeacher(teacher){

        teacher.hired = false;
        this.teachers = this.teachers.filter(teacher => teacher.hired);

        }

    graduate(student){
        student.graduated = true;
        this.students = this.students.filter(student => !student.graduated);
    }

}